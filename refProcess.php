﻿<?php

move_uploaded_file($_FILES['refDigiFile']['tmp_name'], "upload/tif/".$_FILES['refDigiFile']['name']);

$output_string = shell_exec('python parsing_tif.py '.$_FILES['refDigiFile']['name']); 
$output_json = json_decode($output_string);

$tif_info = array();
$tif_info['UL'] = $output_json->UpperRight;
$tif_info['LR'] = $output_json->LowerLeft;
$tif_info['path'] = $output_json->path;

echo json_encode($output_json);

