<?php



$exif = exif_read_data('1.jpg', 0, true);
	
$latitude = $exif['gps']['GPSLatitude'];
$longitude = $exif['gps']['GPSLongitude'];
	
function gps2Num($coordPart) {
	$parts = explode('/', $coordPart);
	if(count($parts) <= 0)
		return 0;
	if(count($parts) == 1)
		return $parts[0];
 
	return floatval($parts[0]) / floatval($parts[1]);
}

function getGps($exifCoord) {
	$degrees = count($exifCoord) > 0 ? gps2Num($exifCoord[0]) : 0;
	$minutes = count($exifCoord) > 1 ? gps2Num($exifCoord[1]) : 0;
	$seconds = count($exifCoord) > 2 ? gps2Num($exifCoord[2]) : 0;
 
	//normalize
	$minutes += 60 * ($degrees - floor($degrees));
	$degrees = floor($degrees);

	$seconds += 60 * ($minutes - floor($minutes));
	$minutes = floor($minutes);

	//extra normalization, probably not necessary unless you get weird data
	if($seconds >= 60) {
		$minutes += floor($seconds/60.0);
		$seconds -= 60*floor($seconds/60.0);
	}

	if($minutes >= 60) {
		$degrees += floor($minutes/60.0);
		$minutes -= 60*floor($minutes/60.0);
	}
	
	return array('degrees' => $degrees, 'minutes' => $minutes, 'seconds' => $seconds);
}

function deg2dec($lat_degObj, $long_degObj) {
	$lat = (float) $lat_degObj['degrees']+((($lat_degObj['minutes']*60)+($lat_degObj['seconds']))/3600);
    $lng = (float) $lat_degObj['degrees']+((($lat_degObj['minutes']*60)+($lat_degObj['seconds']))/3600);
	
	$exif['gps']['GPSLatitudeRef']  == 's' ? $lat *= -1 : '';
    $exif['gps']['GPSLongitudeRef'] == 'w' ? $lng *= -1 : '';
	
	return array('lat' => $lat, 'lng' => $lng);
}

$lat = getGps($latitude);
$long = getGps($longitude);	
$b = deg2dec($lat, $long);
var_dump($b);