<?php 

$gpx = simplexml_load_file("fells_loop.gpx");

$gpxInfo = array();

	if($gpx->attributes()->creator)
		$gpxInfo['creator'] = $gpx->attributes()->creator;
	else
		$gpxInfo['creator'] = '-';
	$gpxInfo['version'] = $gpx->attributes()->version;
	$gpxInfo['time'] = $gpx->time;
	$gpxInfo['bounds'] = array();
		$gpxInfo['bounds']['minlat'] = $gpx->bounds->attributes()->minlat;
		$gpxInfo['bounds']['minlon'] = $gpx->bounds->attributes()->minlon;
		$gpxInfo['bounds']['maxlat'] = $gpx->bounds->attributes()->maxlat;
		$gpxInfo['bounds']['maxlon'] = $gpx->bounds->attributes()->maxlon;
	
	if($gpx->rte) {
		$rte = $gpx->rte;
	
		$rtept = $rte->rtept;
		$rteptCount = count($rtept);
		$gpxInfo['rte'] = array();
		$gpxInfo['rteName'] = (string)$rte->name;
		$gpxInfo['rteCount'] = $rteptCount;
		
		for($i = 0; $i < $rteptCount; $i++) {
			
			array_push($gpxInfo['rte'], array("lat"=>(string)$rtept[$i]->attributes()->lat, "lon"=>(string)$rtept[$i]->attributes()->lon));
		}
		
		echo json_encode($gpxInfo);
	}
	else if($gpx->trk) {
		$trk = $gpx->trk->trkseg;
		
		$trkpt = $trk->trkpt;
		$trkptCount = count($trkpt);
		$gpxInfo['trk'] = array();
		$gpxInfo['trkName'] = 'Track';
		$gpxInfo['trkCount'] = $trkptCount;
		
		for($i = 0; $i < $trkptCount; $i++) {
			
			array_push($gpxInfo['trk'], array("lat"=>(string)$trkpt[$i]->attributes()->lat, "lon"=>(string)$trkpt[$i]->attributes()->lon));
		}
		
		echo json_encode($gpxInfo);
	}
	
		
	
		
	
	