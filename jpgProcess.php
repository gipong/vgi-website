﻿<?php

move_uploaded_file($_FILES['jpgFile']['tmp_name'], "upload/jpg/".$_FILES['jpgFile']['name']);
$exif = exif_read_data("upload/jpg/".$_FILES['jpgFile']['name'], 0, true);

if(isset($exif['GPS'])) {
	$latitude = $exif['GPS']['GPSLatitude'];
	$longitude = $exif['GPS']['GPSLongitude'];
} else {
	$latitude = 0;
	$longitude = 0;
}
	
function gps2Num($coordPart) {
	$parts = explode('/', $coordPart);
	if(count($parts) <= 0)
		return 0;
	if(count($parts) == 1)
		return $parts[0];
 
	return floatval($parts[0]) / floatval($parts[1]);
}

function getGps($exifCoord) {
	$degrees = count($exifCoord) > 0 ? gps2Num($exifCoord[0]) : 0;
	$minutes = count($exifCoord) > 1 ? gps2Num($exifCoord[1]) : 0;
	$seconds = count($exifCoord) > 2 ? gps2Num($exifCoord[2]) : 0;
 
	//normalize
	$minutes += 60 * ($degrees - floor($degrees));
	$degrees = floor($degrees);

	$seconds += 60 * ($minutes - floor($minutes));
	$minutes = floor($minutes);

	//extra normalization, probably not necessary unless you get weird data
	if($seconds >= 60) {
		$minutes += floor($seconds/60.0);
		$seconds -= 60*floor($seconds/60.0);
	}

	if($minutes >= 60) {
		$degrees += floor($minutes/60.0);
		$minutes -= 60*floor($minutes/60.0);
	}
	
	return array('degrees' => $degrees, 'minutes' => $minutes, 'seconds' => $seconds);
}

function deg2dec($lat_degObj, $long_degObj, $exif) {
	$lat = (float) $lat_degObj['degrees']+((($lat_degObj['minutes']*60)+($lat_degObj['seconds']))/3600);
    $lng = (float) $long_degObj['degrees']+((($long_degObj['minutes']*60)+($long_degObj['seconds']))/3600);
	
	if(isset($exif['GPS'])) {
		$exif['GPS']['GPSLatitudeRef']  == 'S' ? $lat *= -1 : '';
		$exif['GPS']['GPSLongitudeRef'] == 'W' ? $lng *= -1 : '';
	}
	
	return array('lat' => $lat, 'lng' => $lng);
}

$lat = getGps($latitude);
$long = getGps($longitude);	
$coords = deg2dec($lat, $long, $exif);

$jpgInfo = array();

$jpgFormData = json_decode($_POST['jpgFormData']);
$jpgInfo['id'] = sha1($jpgFormData->email.substr($jpgFormData->name, 0, 2).time());

$jpgInfo['coords'] = $coords;
$jpgInfo['description'] = $jpgFormData->description;
$jpgInfo['username'] = $jpgFormData->name;
$jpgInfo['useremail'] = $jpgFormData->email;
$jpgInfo['loc'] = "upload/".$_FILES['jpgFile']['name'];


$jpgInfo['FileName'] = $exif['FILE']['FileName'];
$jpgInfo['FileSize'] = $exif['FILE']['FileSize'];
$jpgInfo['MimeType'] = $exif['FILE']['MimeType'];

$jpgInfo['Height'] = $exif['COMPUTED']['Height'];
$jpgInfo['Width'] = $exif['COMPUTED']['Width'];

$jpgInfo['Make'] = $exif['IFD0']['Make'];
$jpgInfo['Model'] = $exif['IFD0']['Model'];
$jpgInfo['Software'] = $exif['IFD0']['Software'];
$jpgInfo['DateTime'] = $exif['IFD0']['DateTime'];

$jpgInfo['DateTimeOriginal'] = $exif['EXIF']['DateTimeOriginal'];

// kml part
$lastUpdate_kml = date("D F d Y h:i:s", time());
$header = "<kml xmlns='http://www.opengis.net/kml/2.2'><Document>\n";
$body = "<Placemark>\n".
    "<name>ID: ".$jpgInfo['id']."</name>\n".
    "<ExtendedData>\n".
		" <Data name='FileName'><value>".$jpgInfo['FileName']."</value></Data>\n".
		" <Data name='FileSize'><value>".$jpgInfo['FileSize']."</value></Data>\n".
		" <Data name='MimeType'><value>".$jpgInfo['MimeType']."</value></Data>\n".
		" <Data name='Height'><value>".$jpgInfo['Height']."</value></Data>\n".
		" <Data name='Width'><value>".$jpgInfo['Width']."</value></Data>\n".
		" <Data name='Make'><value>".$jpgInfo['Make']."</value></Data>\n".
		" <Data name='Model'><value>".$jpgInfo['Model']."</value></Data>\n".
		" <Data name='Software'><value>".$jpgInfo['Software']."</value></Data>\n".
		" <Data name='DateTime'><value>".$jpgInfo['DateTime']."</value></Data>\n".
		" <Data name='DateTimeOriginal'><value>".$jpgInfo['DateTimeOriginal']."</value></Data>\n".
		" <Data name='description'><value>".$jpgFormData->description."</value></Data>\n".
		" <Data name='lastUpdate'><value>".$lastUpdate_kml."</value></Data>\n".
		" <Data name='picture'><value><![CDATA[".
			"<img src='http://140.116.47.109/vgi_web/upload/jpg/".$_FILES['jpgFile']['name']."' width='400' />".
		"]]></value></Data>".
		" <Data name='username'><value>".$jpgInfo['username']."</value></Data>\n".
		" <Data name='useremail'><value>".$jpgInfo['useremail']."</value></Data>\n".
    "</ExtendedData>\n".
    "<Point><coordinates>".$coords['lng'].", ".$coords['lat']."</coordinates></Point></Placemark>\n";
$footer = '</Document></kml>';

$filename = 'jpg'.$jpgInfo['id'].'.kml';

$jpgInfo['fileName'] = $filename;

$file = fopen('./upload/jpg/'.$filename, "w");
fwrite($file, $header.$body.$footer);
fclose($file);

//shell_exec('python jpg2mongodb.py '.json_encode($jpgInfo)); not valid json ?

// insert document to mongoDB
$connection = new MongoClient('mongodb://vgi_igis:vgi_dumbo1017@ds031877.mongolab.com:31877/vgi');

$connection->vgi->jpg->insert(array(
	"geometry" => array(
		"type" => "Point",
		"coordinates" => array($coords['lng'], $coords['lat'])
	),
	"user_info" => array(
		"Name" => $jpgInfo['username'],
		"Email" => $jpgInfo['useremail']
	),
	"automatic" => array(
		"FileName" => $jpgInfo['FileName'],
		"FileSize" => $jpgInfo['FileSize'],
		"MimeType" => $jpgInfo['MimeType'],
		"Height" => $jpgInfo['Height'],
		"Width" => $jpgInfo['Width'],
		"Make" => $jpgInfo['Make'],
		"Model" => $jpgInfo['Model'],
		"Software" => $jpgInfo['Software'],
		"DateTime" => new MongoDate(strtotime($jpgInfo['DateTime'])),
		"DateTimeOriginal" => new MongoDate(strtotime($jpgInfo['DateTimeOriginal']))
	),
	"user_provide" => array(
		"description" => $jpgFormData->description
	),
	"comment" => array(),
	"lastUpdate" => new MongoDate(time()),
	"file_path" => "http://140.116.47.109/vgi_web/upload/jpg/".$filename
));

echo json_encode($jpgInfo);

