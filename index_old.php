<!doctype html>
<html lang="en" ng-app="myApp">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>VGI - angularjs</title>
	<script src="//use.edgefonts.net/arvo.js"></script>
	<script src="http://code.jquery.com/jquery-2.1.0.min.js"></script>
	<script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.0.7/angular.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/js/toastr.min.js"></script>
	<script src="js/semantic.min.js"></script>
    
	<link rel="stylesheet" href="css/semantic.css">
	<link rel="stylesheet" href="css/main.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/css/toastr.min.css">
</head>
<body flow-prevent-drop>
	<div id="attachedButton" class="ui black huge launch right attached button" style="width: 40px;">
		<i class="icon user"></i>
	</div>
	<div class="ui styled sidebar">
		<h3 class="ui header">Try GeoFile platform</h3> 
		<div class="ui form">
		  <div class="field"><label>Account Name</label><div class="ui icon input"><input type="text" placeholder="Account Name"/></div></div>
		  <div class="field"><label>Password</label><div class="ui icon input"><input type="text" placeholder="Password"/></div></div>
		  <div class="ui blue fluid button">Login</div>
		  <div class="ui horizontal divider">Or</div>
		  <div class="ui green fluid button"><i class="signup icon"></i>Sign up</div>
		</div>
	</div>
	
	<div class="ui center aligned two column grid">
		<!-- Menu setting -->
  		<div class="five wide column">
   		   	
   		   	<div class="ui fluid vertical pointing menu" ">
   		   		<div class="header item"><i class="large map marker icon"></i>Mobile device</div>
				
				<a class="blue item" ng-class="{active: active == 'gpx'}" ng-click="active='gpx'; parent='Mobile device'">GPX</a>
				<a class="blue item" ng-class="{active: active == 'georss'}" ng-click="active='georss'; parent='Mobile device'">GeoRSS</a>
				<a class="blue item" ng-class="{active: active == 'jpg'}" ng-click="active='jpg'; parent='Mobile device'" href="#">JPG (with EXIF)</a>
				
				<div class="header item"><i class="large globe icon"></i>Digitalize with Maps</div>
			
				<a class="blue item" ng-class="{active: active == 'kml'}" ng-click="active='kml'; parent='Digitalize with Maps'">KML</a>
				<a class="blue item" ng-class="{active: active == 'kmz'}" ng-click="active='kmz'; parent='Digitalize with Maps'">KMZ</a>
				
				<div class="header item"><i class="large map icon"></i>Existing Geodata</div>
			
				<a class="blue item" ng-class="{active: active == 'shp'}" ng-click="active='shp'; parent='Existing service of Geodata'">Shapefile</a>
				<a class="blue item" ng-class="{active: active == 'gml'}" ng-click="active='gml'; parent='Existing service of Geodata'">GML</a>
				<a class="blue item" ng-class="{active: active == 'geocoding'}" ng-click="active='geocoding'; parent='Existing service of Geodata'">Geocoding service</a>

			</div>
   		   	
   		</div>
  		
  		<!-- Content view -->
		<div class="nine wide column">
		<!--
			<div class="ui left breadcrumb" ng-show="active">
				{{parent}}  <i class="right arrow icon divider"></i>  {{active}}
			</div>
		-->
			<!-- GPX content -->
			<div class="ui piled segment" ng-show="active == 'gpx'" ng-controller="gpsFormController"`>
			
				<h2 class="ui header">{{active | uppercase}}</h2>
				<div class="ui blue ribbon label" style="position: absolute; left: -1rem; z-index: 10;">Intro</div>
 				<div class="ui horizontal icon divider"><i class="circular list icon"></i></div>
				<i class="quote left icon"></i>
				GPX, or GPS eXchange Format is an XML schema designed as a common GPS data format for software applications.
				<i class="quote right icon"></i> - Wikipedia
				<br /><h2></h2>
			
 				<div class="ui blue ribbon label" style="position: absolute; left: -1rem; z-index: 10;">File upload</div>
				<div class="ui horizontal icon divider"><i class="circular attachment icon"></i></div>
				<br />
 					<!-- <div class="ui success message" ng-show="message"><i class="close icon" ng-click="cancel()"></i>{{message}}</div> -->
					
 					<form name="gpsForm" ng-submit="processForm()" class="ui fluid form">
						<!-- File preview -->
						<table class="ui table segment">
						  <thead><tr><th><i class="file icon"></i>File</th><th><i class="info icon"></i>Size</th></tr></thead>
						  <tbody>
							<tr><td><pre ng-show="files.length > 0" class="trim">{{files[files.length-1].name}}</pre></td>
							<td><pre ng-show="files.length > 0">{{files[files.length-1].size}}</pre></td></tr>
						  </tbody>
						  <tfoot>
							<tr><th colspan="3">
							  <div><label for="gpsFile" class="home positive ui button"><i class="icon upload"></i>Add File</label><input id="gpsFile" type="file" file-upload style="display: none;">
							  <label class="ui red button" ng-click="files.length = 0"><i class="icon remove"></i>Cancel</label></div>
							</th>
						  </tr></tfoot>
						</table>
						
						
						<h2></h2>
						<div class="ui blue ribbon label" style="position: absolute; z-index: 10;">Metadata</div>
						<div class="ui horizontal icon divider"><i class="circular info letter icon"></i></div>
						<div class="field">
							<label>E-mail</label>
							<div class="ui left labeled icon input">
								<input type="email" name="email" placeholder="email" ng-model="gpsFormData.email" required><i class="user icon"></i>
								<div ng-show="gpsForm.email.$invalid && !gpsForm.email.$pristine" class="ui red pointing above ui label">請輸入E-mail</div>
							</div>
							
						</div>
						<div class="field">
							<label>Password</label>
							<div class="ui left labeled icon input">
								<input type="password" name="password" placeholder="password" ng-model="gpsFormData.password" ng-minlength="5" required><i class="lock icon"></i>
								<div ng-show="gpsForm.password.$error.minlength" class="ui red pointing above ui label">請輸入超過5個字元以上的密碼</div>
							</div>
						</div>
						<br />
						<div class="field">
						  <div class="ui fluid accordion">
							<div class="title" style="text-align: left;"><i class="icon dropdown"></i>Optional Field</div>
							<div class="content field">
								<label>How are you?</label>
								<input placeholder="How are you?" type="email">
							</div>
						  </div>
						</div>
						<button type="submit"  ng-disabled="gpsForm.$invalid" ng-class="{disabled: gpsForm.$invalid}" class="ui blue submit button">Submit</button>
					</form>
				<br />
					
				<!-- output display -->
				<div ng-show="output" class="ui left">
					<div class="ui red ribbon label" style="position: absolute; left: -1rem; z-index: 10;">Output</div><br /><br />
					<div class="ui list">
					  <div class="list">
						<div class="item" >
							<div class="ui label">
							  ID<div class="detail">{{field.id}}</div>
							</div>
						</div>
						<div class="item" ng-show="field.creator[0]"><i class="triangle right icon"></i>
							<div class="content" style="text-align: left;">
							  <div class="header">Creator</div><div class="description">{{field.creator[0]}}</div>
							</div>
						</div>
						<div class="item" ng-show="field.rteName"><i class="triangle right icon"></i>
							<div class="content" style="text-align: left;">
							  <div class="header">Route Name</div><div class="description">{{field.rteName}}</div>
							</div>
						</div>
						<div class="item" ng-show="field.version[0]"><i class="triangle right icon"></i>
							<div class="content" style="text-align: left;">
							  <div class="header">GPX version</div><div class="description">{{field.version[0]}}</div>
							</div>
						</div>
						<div class="item" ng-show="field.time[0]"><i class="triangle right icon"></i>
							<div class="content" style="text-align: left;">
							  <div class="header">Time</div><div class="description">{{field.time[0]}}</div>
							</div>
						</div>
						
						<div class="item" ng-show="field.bounds"><i class="triangle right icon"></i>
							<div class="content" style="text-align: left;">
							  <div class="header">Bounds</div><div class="description">
							  MinLat {{field.bounds.minlat[0]}}<br />MinLon {{field.bounds.minlon[0]}}<br />MaxLat {{field.bounds.maxlat[0]}}<br />MaxLon{{field.bounds.maxlon[0]}}
							  </div>
							</div>
						</div>
						<br />
						<button class="ui teal secondary button" id="showGpsMap"><i class="cloud upload icon"></i>View on Map</button>
					  </div>
					</div>
					
				</div>
				<div class="ui modal">
				  <i class="close icon"></i>
				  <div class="header">View on Map</div>
				  <div class="content"><div id="map_canvas" style="width: 100%;height:400px;"></div></div>
				  <div class="actions"><div class="ui button">Cancel</div></div>
				</div>
			</div>
			
			<!-- GeoRSS content -->
			<div class="ui piled segment" ng-show="active == 'georss'">
				<h2>{{active | uppercase}}</h2>
 				<p>this is for georss</p>
			</div>
			
			<!-- JPG content -->
			<div class="ui piled segment" ng-show="active == 'hjpg'">
				<h2 class="ui header">{{active | uppercase}}</h2>
				<div class="ui blue ribbon label" style="position: absolute; left: -1rem; z-index: 10;">Intro</div>
 				<div class="ui horizontal icon divider"><i class="circular list icon"></i></div>
				<i class="quote left icon"></i>
				JPEG compression is used in a number of image file formats. JPEG/Exif is the most common image format used by digital cameras and other photographic image capture devices; along with JPEG/JFIF, it is the most common format for storing and transmitting photographic images on the World Wide Web.[citation needed] These format variations are often not distinguished, and are simply called JPEG.
				<i class="quote right icon"></i> - Wikipedia
				<br /><h2></h2>
			
 				<div class="ui blue ribbon label" style="position: absolute; left: -1rem; z-index: 10;">File upload</div>
				<div class="ui horizontal icon divider"><i class="circular attachment icon"></i></div>
				<br />
				
 					<form name="jpgForm" ng-submit="jpgProcessForm()" class="ui fluid form">
						<!-- File preview -->
						<table class="ui table segment">
						  <thead><tr><th><i class="file icon"></i>File</th><th><i class="info icon"></i>Size</th></tr></thead>
						  <tbody>
							<tr><td><pre ng-show="files.length > 0" class="trim">{{files[files.length-1].name}}</pre></td>
							<td><pre ng-show="files.length > 0">{{files[files.length-1].size}}</pre></td></tr>
						  </tbody>
						  <tfoot>
							<tr><th colspan="3">
							  <div><label for="jpgFile" class="home positive ui button"><i class="icon upload"></i>Add File</label><input id="jpgFile" type="file" file-upload style="display: none;">
							  <label class="ui red button" ng-click="files.length = 0"><i class="icon remove"></i>Cancel</label></div>
							</th>
						  </tr></tfoot>
						</table>
						
						
						<h2></h2>
						<div class="ui blue ribbon label" style="position: absolute; z-index: 10;">Metadata</div>
						<div class="ui horizontal icon divider"><i class="circular info letter icon"></i></div>
						<div class="field">
							<label>E-mail</label>
							<div class="ui left labeled icon input">
								<input type="email" name="email" placeholder="email" ng-model="jpgFormData.email" required><i class="user icon"></i>
								<div ng-show="jpgForm.email.$invalid && !jpgForm.email.$pristine" class="ui red pointing above ui label">請輸入E-mail</div>
							</div>
							
						</div>
						<div class="field">
							<label>Password</label>
							<div class="ui left labeled icon input">
								<input type="password" name="password" placeholder="password" ng-model="jpgFormData.password" ng-minlength="5" required><i class="lock icon"></i>
								<div ng-show="jpgForm.password.$error.minlength" class="ui red pointing above ui label">請輸入超過5個字元以上的密碼</div>
							</div>
						</div>
						<br />
						<div class="field">
						  <div class="ui fluid accordion">
							<div class="title" style="text-align: left;"><i class="icon dropdown"></i>Optional Field</div>
							<div class="content field">
								<label>How are you?</label>
								<input placeholder="How are you?" type="email">
							</div>
						  </div>
						</div>
						<button type="submit"  ng-disabled="jpgForm.$invalid" ng-class="{disabled: jpgForm.$invalid}" class="ui blue submit button">Submit</button>
					</form>
				<br />
				
				
				
			</div>

			<!-- KML content -->
			<div class="ui piled segment" ng-show="active == 'kml'">
				<h2>{{active | uppercase}}</h2>
 				<p>this is for georss</p>
			</div>
			
			<!-- KMZ content -->
			<div class="ui piled segment" ng-show="active == 'kmz'">
				<h2>{{active}}</h2>
 				<p>this is for georss</p>
			</div>
			
			<!-- SHP content -->
			<div class="ui piled segment" ng-show="active == 'shp'">
				<h2>{{active | uppercase}}</h2>
 				<p>this is for georss</p>
			</div>
			
			<!-- GML content -->
			<div class="ui piled segment" ng-show="active == 'gml'">
				<h2>{{active}}</h2>
 				<p>this is for georss</p>
			</div>
			
			<!-- Geocoding content -->
			<div class="ui piled segment" ng-show="active == 'geocoding'">
				<h2>{{active}}</h2>
 				<p>this is for georss</p>
			</div>

		</div>
		<div class="ng-view"></div>
    </div>
	<script src="js/main.js"></script>
	<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
   
</body>

<script>
/*
	$('.ui.form').form({
		username: {
			identifier: 'username',
			rules: [{ type: 'empty', prompt: 'please enter your given name' }]
		},
		password: {
			identifier: 'password',
			rules: [{ type: 'empty', prompt: 'please enter your password' },
					{ type: 'length[6]', prompt: 'your password must be at least 6 chars' }]
		}
	}, { on: 'blur', inline: 'true'});
*/

	</script>
</html>