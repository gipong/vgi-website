import gdal, sys, os, json
from gdalconst import *
import numpy

fn = sys.argv[1]
fn_name = sys.argv[1].split('.')[0]
#fn = '94184009.tif'

os.system('gdalwarp \
-s_srs "+proj=tmerc +lat_0=0 +lon_0=121 +k=0.9999 +x_0=250000 +y_0=0 +ellps=aust_SA +units=m +no_defs" \
-t_srs "+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs" \
upload/tif/'+fn+' upload/tif/'+fn+'-EPSG4826.tif >upload/tif/system.txt')

os.system('gdal_translate -of JPEG upload/tif/'+fn+'-EPSG4826.tif upload/tif/'+fn_name+'-EPSG4826.jpg >upload/tif/system.txt')

ref_list = {}

gdal.AllRegister()
ds = gdal.Open('upload/tif/'+fn+'-EPSG4826.tif', GA_ReadOnly)
width = ds.RasterXSize 
height = ds.RasterYSize 

geotransform = ds.GetGeoTransform()
ref_list['LowerLeft'] = (geotransform[0], geotransform[3] + width*geotransform[4] + height*geotransform[5] )
ref_list['UpperRight'] = (geotransform[0] + width*geotransform[1] + height*geotransform[2], geotransform[3])
ref_list['path'] = 'upload/tif/'+fn_name+'-EPSG4826.jpg'

print json.dumps(ref_list)

