﻿<?php
move_uploaded_file($_FILES['shpFile_shp']['tmp_name'], "upload/shp/".$_FILES['shpFile_shp']['name']);
move_uploaded_file($_FILES['shpFile_shx']['tmp_name'], "upload/shp/".$_FILES['shpFile_shx']['name']);
move_uploaded_file($_FILES['shpFile_dbf']['tmp_name'], "upload/shp/".$_FILES['shpFile_dbf']['name']);

$output_string = shell_exec('python parsing_shp.py'.$_FILES['shpFile_shp']['name']); 
$output_string = shell_exec('python parsing_shp.py banyan.shp'); 
$output_json = json_decode($output_string);
$shpFormData = json_decode($_POST['shpFormData']);
$shpInfo = array();


$shpInfo['id'] = sha1($shpFormData->email.substr($shpFormData->password, 0, 3).time());
$shpInfo['fileString'] = $output_string;

$shpInfo['extent'] = array(
	'UL'=>array($output_json->extent[0], $output_json->extent[3]),
	'LR'=>array($output_json->extent[1], $output_json->extent[2])
	);
$shpInfo['numFeatures'] = $output_json->numFeatures;
$shpInfo['rowInfo'] = array();

for($i = 0;$i < $output_json->numFeatures; $i++) {
	$shpInfo['rowInfo'][$i] = array(
		'Type' => $output_json->rowInfo->fid0[0],
		'Lat' => $output_json->rowInfo->fid0[2],
		'Lng' => $output_json->rowInfo->fid0[1],
	);
}


echo json_encode($shpInfo);

