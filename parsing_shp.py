#!C:/Python27/python.exe
from osgeo import gdal, gdalconst
from osgeo import ogr
import sys, os, json


#os.chdir('/upload/shp');
driver = ogr.GetDriverByName('ESRI Shapefile');

fn = 'upload/shp/' + sys.argv[1]
#fn = 'upload/shp/banyan.shp'
dataSource = driver.Open(fn, 0) # 0 read-only , 1 for writeable
if dataSource is None:
    print 'Could not open ' + fn

py_list = {}
    
layer = dataSource.GetLayer()
numFeatures = layer.GetFeatureCount()
py_list['numFeatures'] = numFeatures
#print 'Feature count:' + str(numFeatures)

extent = layer.GetExtent()
py_list['extent'] = extent
#print 'Extent:' , extent
#print 'UL:' , extent[0], extent[3]
#print 'LR:' , extent[1], extent[2]    

'''
feature = layer.GetNextFeature()
geometry = feature.GetGeometryRef()
print 'Geometry:' , geometry.GetGeometryName()

while feature:
    feature = layer.GetNextFeature()
    geometry = feature.GetGeometryRef()
    print 'Geometry:' , geometry.GetGeometryName()
'''
py_list['rowInfo'] = {}
for i in range(layer.GetFeatureCount()):
    feature = layer.GetFeature(i)
    id = feature.GetField("id")
    geometry = feature.GetGeometryRef()
    #print i, id, geometry.GetGeometryName()
    if geometry.GetGeometryName() == "POINT":
        py_list['rowInfo']['fid'+str(id)] = (geometry.GetGeometryName(), geometry.GetX(), geometry.GetY())
    else:    
        py_list['rowInfo']['fid'+str(id)] = (geometry.GetGeometryName())
    
   
#print py_list

print json.dumps(py_list)
