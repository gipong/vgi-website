﻿<?php

//move_uploaded_file($_FILES['jpgFile']['tmp_name'], "upload/jpg/".$_FILES['jpgFile']['name']);
$kmlFormData = json_decode($_POST['kmlFormData']);
$kmlInfo = array();

$kmlInfo['id'] = sha1($kmlFormData->email.substr($kmlFormData->name, 0, 2).time());
$kmlInfo['Lat'] = $kmlFormData->Lat;
$kmlInfo['Lng'] = $kmlFormData->Lng;
$kmlInfo['MapType'] = $kmlFormData->MapType;
$kmlInfo['ZoomLevel'] = $kmlFormData->ZoomLevel;
$kmlInfo['MapSource'] = $kmlFormData->MapSource;
$kmlInfo['ImgSource'] = $kmlFormData->ImgSource;

// kml part
$lastUpdate_kml = date("D F d Y h:i:s", time());
$header = "<kml xmlns='http://www.opengis.net/kml/2.2'><Document>\n";
$body = "<Placemark>\n".
    "<name>ID: ".$kmlInfo['id']."</name>\n".
    "<ExtendedData>\n".
		" <Data name='MapType'><value>".$kmlInfo['MapType']."</value></Data>\n".
		" <Data name='MapSource'><value>".$kmlInfo['MapSource']."</value></Data>\n".
		" <Data name='ZoomLevel'><value>".$kmlInfo['ZoomLevel']."</value></Data>\n".
		" <Data name='ImgSource'><value>".$kmlInfo['ImgSource']."</value></Data>\n".
		" <Data name='description'><value>".$kmlFormData->description."</value></Data>\n".
		" <Data name='lastUpdate'><value>".$lastUpdate_kml."</value></Data>\n".
		" <Data name='username'><value>".$kmlFormData->name."</value></Data>\n".
		" <Data name='useremail'><value>".$kmlFormData->email."</value></Data>\n".
    "</ExtendedData>\n".
    "<Point><coordinates>".$kmlInfo['Lng'].", ".$kmlInfo['Lat']."</coordinates></Point></Placemark>\n";
$footer = '</Document></kml>';

$filename = 'kml'.$kmlInfo['id'].'.kml';

$kmlInfo['fileName'] = $filename;

$file = fopen('./upload/kml/'.$filename, "w");
fwrite($file, $header.$body.$footer);
fclose($file);

// insert document to mongoDB
$connection = new MongoClient('mongodb://vgi_igis:vgi_dumbo1017@ds031877.mongolab.com:31877/vgi');

$connection->vgi->kml->insert(array(
	"geometry" => array(
		"type" => "Point",
		"coordinates" => array($kmlInfo['Lng'], $kmlInfo['Lat'])
	),
	"user_info" => array(
		"Name" => $kmlFormData->name,
		"Email" => $kmlFormData->email
	),
	"automatic" => array(
		"FileName" => $kmlInfo['fileName'],
		"MapType" => $kmlInfo['MapType'],
		"ZoomLevel" => $kmlInfo['ZoomLevel'],
		"MapSource" => $kmlInfo['MapSource'],
		"ImgSource" => $kmlInfo['ImgSource']
	),
	"user_provide" => array(
		"description" => $kmlFormData->description
	),
	"comment" => array(),
	"lastUpdate" => new MongoDate(time()),
	"file_path" => "http://140.116.47.109/vgi_web/upload/kml/".$filename
));


echo json_encode($kmlInfo);

