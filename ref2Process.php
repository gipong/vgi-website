﻿<?php

//move_uploaded_file($_FILES['jpgFile']['tmp_name'], "upload/jpg/".$_FILES['jpgFile']['name']);
$refFormData = json_decode($_POST['refFormData']);
$refInfo = array();

$refInfo['id'] = sha1($refFormData->email.substr($refFormData->password, 0, 3).time());
$refInfo['Lat'] = $refFormData->Lat;
$refInfo['Lng'] = $refFormData->Lng;
$refInfo['MapType'] = $refFormData->MapType;
$refInfo['ZoomLevel'] = $refFormData->ZoomLevel;
$refInfo['MapSource'] = $refFormData->MapSource;
$refInfo['ImgSource'] = $refFormData->ImgSource;

$refInfo['path'] = $refFormData->path;


// kml part
$lastUpdate_kml = date("D F d Y h:i:s", time());
$header = "<kml xmlns='http://www.opengis.net/kml/2.2'><Document>\n";
$body = "<Placemark>\n".
    "<name>ID: ".$refInfo['id']."</name>\n".
    "<ExtendedData>\n".
		" <Data name='MapType'><value>".$refInfo['MapType']."</value></Data>\n".
		" <Data name='MapSource'><value>".$refInfo['MapSource']."</value></Data>\n".
		" <Data name='ZoomLevel'><value>".$refInfo['ZoomLevel']."</value></Data>\n".
		" <Data name='ImgSource'><value>".$refInfo['ImgSource']."</value></Data>\n".
		" <Data name='lastUpdate'><value>".$lastUpdate_kml."</value></Data>\n".
    "</ExtendedData>\n".
    "<Point><coordinates>".$refInfo['Lng'].", ".$refInfo['Lat']."</coordinates></Point></Placemark>\n".
	"<GroundOverlay id='userRefMap".$refInfo['id']."'>\n".
		"<Icon><href>http://140.116.47.109/vgi_web/".$refInfo['path']."</href></Icon>\n".
		"<LatLonBox>\n".
		"<north>".$refFormData->UpperRight[1]."</north>\n".
		"<south>".$refFormData->LowerLeft[1]."</south>\n".
		"<east>".$refFormData->UpperRight[0]."</east>\n".
		"<west>".$refFormData->LowerLeft[0]."</west>\n".
		"<rotation>0</rotation>\n".
		"</LatLonBox>\n".
	"</GroundOverlay>\n";
$footer = '</Document></kml>';

$filename = 'ref'.$refInfo['id'].'.kml';

$refInfo['fileName'] = $filename;

$file = fopen('./upload/tif/'.$filename, "w");
fwrite($file, $header.$body.$footer);
fclose($file);

// insert document to mongoDB


echo json_encode($refInfo);

