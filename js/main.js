﻿var myApp = angular.module('myApp', []);

myApp.config(['$routeProvider', '$locationProvider', 
	function($routeProvider, $locationProvider) {
		$routeProvider.
			when('/jpg', {
			  templateUrl: 'templates/jpg.html',
			  controller: 'jpgFormController'
			}).
			when('/gpx', {
			  templateUrl: 'templates/gpx.html',
			  controller: 'gpsFormController'
			}).
			when('/kml', {
			  templateUrl: 'templates/kml.html',
			  controller: 'kmlFormController'
			}).
			when('/shp', {
			  templateUrl: 'templates/shp.html',
			  controller: 'shpFormController'
			}).
			when('/ref', {
			  templateUrl: 'templates/ref.html',
			  controller: 'refFormController'
			}).
			otherwise({
				redirectTo: '/index.html'
			});
			
		// use the HTML5 History API
		$locationProvider.html5Mode(true);
	}]);

$('.ui.dropdown').dropdown();
	
myApp.directive('fileUpload', function () {
    return {
        scope: true,
        link: function (scope, element, attrs) {
            element.bind('change', function (event) {
                var files = event.target.files;
                for (var i = 0;i<files.length;i++) {
                    scope.$emit("fileSelected", { file: files[i] });
                } 
            });
        }
    };
});

toastr.options = {
  "closeButton": true,
  "positionClass": "toast-bottom-left"
 };
$('.styled.sidebar').first().sidebar('attach events', '.attached.button');
$('.attached.button').removeClass('disabled');;



myApp.controller('gpsFormController', function($scope, $http) {
	$scope.gpsFormData = {};
	$scope.files = [];
	
	$scope.output = false;
	$scope.loading = false;
	$scope.field = {};
	
	$scope.previewGpx;
	function preview(xml) {
		var parser = new DOMParser();
		var gpxXml = parser.parseFromString(xml, "text/xml");
		var mapOptions = {
		  center: new google.maps.LatLng(0, 0),
		  zoom: 8,
		  mapTypeId: google.maps.MapTypeId.ROADMAP
		};
		var map = new google.maps.Map(document.getElementById("map_preview"), mapOptions);
		var points = [];
		var bounds = new google.maps.LatLngBounds();
		if($(gpxXml).find("trkpt")[0] != null) {
			$(gpxXml).find("trkpt").each(function() {
				var lat = $(this).attr("lat");
				var lon = $(this).attr("lon");
				
				var trkpt = new google.maps.LatLng(lat, lon);
				points.push(trkpt);
				bounds.extend(trkpt);
			});
		} else if($(gpxXml).find("rtept")[0] != null) {
			$(gpxXml).find("rtept").each(function() {
				var lat = $(this).attr("lat");
				var lon = $(this).attr("lon");
				
				var rtept = new google.maps.LatLng(lat, lon);
				points.push(rtept);
				bounds.extend(rtept);
			});
		}		
		
		var poly = new google.maps.Polyline({
			path: points,
			strokeColor: '#0080FF',
			strokeOpacity: 1.0,
			strokeWeight: 2
		});
		
		poly.setMap(map);
		map.fitBounds(bounds);
		console.log();
	}
	
	$('.ui.fluid.accordion').accordion();
	
	function initialize(gpx) {
		var mapOptions = {
		  center: new google.maps.LatLng(0, 0),
		  zoom: 8,
		  mapTypeId: google.maps.MapTypeId.ROADMAP
		};
		var bound = new google.maps.LatLngBounds();
		var gpsBoundaryPath = [];
		var map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
		
		var gpsPath = [];
		
		if(gpx.rte) {
			$.each(gpx.rte, function(index, value) {
				gpsPath.push(new google.maps.LatLng(parseFloat(value['lat']), parseFloat(value['lon'])));
				//console.log(value['lat']);
			});
		}
		else if(gpx.trk) {
			$.each(gpx.trk, function(index, value) {
				gpsPath.push(new google.maps.LatLng(parseFloat(value['lat']), parseFloat(value['lon'])));
				//console.log(value['lat']);
				bound.extend(new google.maps.LatLng(parseFloat(value['lat']), parseFloat(value['lon'])));
			});
		}
		
		if(gpx.bounds) {
			bound.extend(new google.maps.LatLng(parseFloat(gpx.bounds.minlat[0]), parseFloat(gpx.bounds.minlon[0])));
			bound.extend(new google.maps.LatLng(parseFloat(gpx.bounds.maxlat[0]), parseFloat(gpx.bounds.maxlon[0])));
		
			var gpsBoundaryPath = [
				new google.maps.LatLng(parseFloat(gpx.bounds.minlat[0]), parseFloat(gpx.bounds.minlon[0])),
				new google.maps.LatLng(parseFloat(gpx.bounds.maxlat[0]), parseFloat(gpx.bounds.minlon[0])),
				new google.maps.LatLng(parseFloat(gpx.bounds.maxlat[0]), parseFloat(gpx.bounds.maxlon[0])),
				new google.maps.LatLng(parseFloat(gpx.bounds.minlat[0]), parseFloat(gpx.bounds.maxlon[0]))
			];

		}else {
			var gpsBoundaryPath = [
				bound.getSouthWest(),
				new google.maps.LatLng(bound.getNorthEast().lat(), bound.getSouthWest().lng()),
				bound.getNorthEast(),
				new google.maps.LatLng(bound.getSouthWest().lat(), bound.getNorthEast().lng())
			];
		}
		
		gpxBounds = new google.maps.Polygon({
			paths: gpsBoundaryPath,
			strokeColor: '#FF0000',
			strokeOpacity: 0.8,
			strokeWeight: 3,
			fillColor: '#FF0000',
			fillOpacity: 0.05
		});
		
		gpxRoute = new google.maps.Polyline({
			path: gpsPath,
			strokeColor: '#0080FF',
			strokeOpacity: 1.0,
			strokeWeight: 2
		});

		map.fitBounds(bound);
		gpxBounds.setMap(map);
		gpxRoute.setMap(map);
	}
	
	$('#showGpsMap').click(function() { $('.ui.modal.gps').modal('setting', 'transition', 'vertical flip').modal('show'); initialize($scope.field); });
	$('#showGpsPreviewMap').click(function() { $('.ui.modal.gpsPreview').modal('setting', 'transition', 'vertical flip').modal('show'); preview($scope.previewGpx)});
	$scope.$on("fileSelected", function (event, args) {
		$scope.$apply(function () {
			console.log(args);
			if(args.file.name.split(".")[1] != 'gpx')
				toastr.error('請挑選.gpx格式進行上傳.', 'Error Info');
			else
				$scope.files.push(args.file);
				var gpxReader = new FileReader();
				gpxReader.onload = function(event) {
					$scope.previewGpx = event.target.result;
					console.log(event.target);					
				};
				gpxReader.readAsText(args.file);
		});
	})
	
	$scope.gpsProcessForm = function() {
		$http({
			method: 'POST',
			url: 'gpsProcess.php',
			transformRequest: function (data) {
				var formdata = new FormData();
				formdata.append("gpsFormData", angular.toJson(data.gpsFormData));
				formdata.append("gpsFile", data.gpsFile);

				/*
				for(var i=0; i <data.files; i++) {
					formdata.append("gpsFile"+i, data.files[i])
				}
				*/
				
				return formdata;
			},
			//data: $.param($scope.formData),
			data: {gpsFormData: $scope.gpsFormData, gpsFile: $scope.files[0] },
			headers : { 'Content-Type': false }
		})
			.success(function(data) {
				
				console.log(data);
				$scope.output = true;
				$scope.loading = false;
				$scope.field = data;
				toastr.success('您的GPX資料已解析完成，請至下方Output標籤內觀看資訊.', 'Info');
				
			});
	};

});

myApp.controller('jpgFormController', function($scope, $http) {
	
	function setInit(open, close, day) {
		if($("#"+day).is(":checked")) {
			var opentime = '00';
			if(open-Math.floor(open)>0)
				opentime = '30';
			var closetime = '00';
			if(close-Math.floor(close)>0)
				closetime = '30';
			return Math.floor(open)+':'+opentime+'-'+Math.floor(close)+':'+closetime;
		} else {
			return "Closed"
		}
	}
	
	$("#range-slider-1").slider({
		range: true,
		min: 0,
		max: 24,
		values: [8, 20],
		step:0.5,
		slide: function(event, ui) {
			$("#range-time-1").val(setInit(ui.values[0], ui.values[1], 'monday'));
		}
	});
	$("#range-time-1").val(
		setInit($("#range-slider-1").slider("values", 0), $("#range-slider-1").slider("values", 1), 'monday')
	);
	$("#range-slider-2").slider({
		range: true,
		min: 0,
		max: 24,
		values: [8, 20],
		step:0.5,
		slide: function(event, ui) {
			$("#range-time-2").val(setInit(ui.values[0], ui.values[1], "tuesday"));
		}
	});
	$("#range-time-2").val(
		setInit($("#range-slider-2").slider("values", 0), $("#range-slider-2").slider("values", 1), "tuesday")
	);
	$("#range-slider-3").slider({
		range: true,
		min: 0,
		max: 24,
		values: [8, 20],
		step:0.5,
		slide: function(event, ui) {
			$("#range-time-3").val(setInit(ui.values[0], ui.values[1], "wednesday"));
		}
	});
	$("#range-time-3").val(
		setInit($("#range-slider-3").slider("values", 0), $("#range-slider-3").slider("values", 1), "wednesday")
	);
	$("#range-slider-4").slider({
		range: true,
		min: 0,
		max: 24,
		values: [8, 20],
		step:0.5,
		slide: function(event, ui) {
			$("#range-time-4").val(setInit(ui.values[0], ui.values[1], "thursday"));
		}
	});
	$("#range-time-4").val(
		setInit($("#range-slider-4").slider("values", 0), $("#range-slider-4").slider("values", 1), "thursday")
	);
	$("#range-slider-5").slider({
		range: true,
		min: 0,
		max: 24,
		values: [8, 20],
		step:0.5,
		slide: function(event, ui) {
			$("#range-time-5").val(setInit(ui.values[0], ui.values[1], "friday"));
		}
	});
	$("#range-time-5").val(
		setInit($("#range-slider-5").slider("values", 0), $("#range-slider-5").slider("values", 1), "friday")
	);
	$("#range-slider-6").slider({
		range: true,
		min: 0,
		max: 24,
		values: [8, 20],
		step:0.5,
		slide: function(event, ui) {
			$("#range-time-6").val(setInit(ui.values[0], ui.values[1], "saturday"));
		}
	});
	$("#range-time-6").val(
		setInit($("#range-slider-6").slider("values", 0), $("#range-slider-6").slider("values", 1), "saturday")
	);
	$("#range-slider-7").slider({
		range: true,
		min: 0,
		max: 24,
		values: [8, 20],
		step:0.5,
		slide: function(event, ui) {
			$("#range-time-7").val(setInit(ui.values[0], ui.values[1], "sunday"));
		}
	});
	$("#range-time-7").val(
		setInit($("#range-slider-7").slider("values", 0), $("#range-slider-7").slider("values", 1), "sunday")
	);
	

	$scope.jpgFormData = {};
	$scope.files = [];
	
	$scope.output = false;
	$scope.loading = false;
	$scope.field = {};
	
	$('.ui.fluid.accordion').accordion();
	
	function initialize(jpg) {
		
		var myLatlng = new google.maps.LatLng(parseFloat(jpg.coords['lat']), parseFloat(jpg.coords['lng']));
		var mapOptions = {
		  center: myLatlng,
		  zoom: 12,
		  mapTypeId: google.maps.MapTypeId.ROADMAP
		};
		var map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
		var marker = new google.maps.Marker({
		  position: myLatlng,
		  animation: google.maps.Animation.DROP,
		  map: map,
		  title: 'Marker'
		});
		
		var markContent = 
		'<div class="ui items" style="overflow: hidden;">'+
			'<div class="item">'+
			'<div class="image">'+
				'<img src="'+ jpg.loc +'">'+
			'</div>'+
		'<div class="content">'+
		'<div class="name">Picture</div>'+
		'<p class="description">'+ jpg.description +'</p></div>';
		
		var infowindow = new google.maps.InfoWindow({
			content: markContent
		});
		
		google.maps.event.addListener(marker, 'click', function() {
			infowindow.open(map, marker);
		});

	}
	
	$('#showJpgMap').click(function() { $('.ui.modal').modal('setting', 'transition', 'vertical flip').modal('show'); initialize($scope.field); });
	
	$scope.$on("fileSelected", function (event, args) {
		$scope.$apply(function () {
			console.log(args);
			if(args.file.name.split(".")[1] != 'jpg')
				toastr.error('請挑選.jpg或.tiff格式進行上傳.', 'Error Info');
			else
				$scope.files.push(args.file);
		});
	})
	 
	$scope.jpgProcessForm = function() {
		$http({
			method: 'POST',
			url: 'jpgProcess.php',
			transformRequest: function (data) {
				var formdata = new FormData();
				formdata.append("jpgFormData", angular.toJson(data.jpgFormData));
				formdata.append("jpgFile", data.jpgFile);

				/*
				for(var i=0; i <data.files; i++) {
					formdata.append("gpsFile"+i, data.files[i])
				}
				*/
				
				return formdata;
			},
			//data: $.param($scope.formData),
			data: {jpgFormData: $scope.jpgFormData, jpgFile: $scope.files[0] },
			headers : { 'Content-Type': false }
		})
			.success(function(data) {
				
				console.log(data);
				$scope.output = true;
				$scope.loading = false;
				$scope.field = data;
				if(data.coords.lat == 0 && data.coords.lng == 0) {
					toastr.warning('您的JPEG未具有GPS標籤.', 'Warning!');
					console.log(data.coords.lat);
				}
				toastr.success('您的JPEG資料已解析完成，請至下方Output標籤內觀看資訊.', 'Info');
				
			});
	};
	
});

myApp.controller('kmlFormController', function($scope, $http) {
	$scope.kmlFormData = {};
	$scope.files = [];
	
	$scope.output = false;
	$scope.field = {};
	
	$('.ui.fluid.accordion').accordion();
	
	
		
	var myLatlng = new google.maps.LatLng(22.9974, 120.2182);
	var mapOptions = {
	  center: myLatlng,
	  zoom: 12,
	  mapTypeId: google.maps.MapTypeId.ROADMAP
	};
	var map_digitalize = new google.maps.Map(document.getElementById("map_canvas_digitalize"), mapOptions);
	var marker_digitalize = new google.maps.Marker({
	  position: myLatlng,
	  draggable:true,
	  animation: google.maps.Animation.BOUNCE,
	  map: map_digitalize,
	  title: 'Place'
	});
	/*
	var markContent = '';
	
	var infowindow = new google.maps.InfoWindow({
		content: markContent
	});
	
	google.maps.event.addListener(marker, 'click', function() {
		infowindow.open(map, marker);
	});
	*/
	
	function update(e) {
		$scope.Lat = e.getPosition().lat();
		$scope.Lng = e.getPosition().lng();
		$scope.ZoomLevel = map_digitalize.getZoom();
		$scope.MapType = map_digitalize.getMapTypeId();
		$scope.ImgSource = '-';
		$scope.MapSource = '2014 Google';
		
		$scope.kmlFormData.Lat = e.getPosition().lat();
		$scope.kmlFormData.Lng = e.getPosition().lng();
		$scope.kmlFormData.ZoomLevel = map_digitalize.getZoom();
		$scope.kmlFormData.MapType = map_digitalize.getMapTypeId();
		$scope.kmlFormData.ImgSource = '-';
		$scope.kmlFormData.MapSource = '2014 Google';
	}
	
	update(marker_digitalize);
	
	google.maps.event.addListener(marker_digitalize, 'dragend', function() {
		map_digitalize.panTo(this.getPosition());
		
		document.getElementById("Lat").innerHTML = this.getPosition().lat();
		$scope.kmlFormData.Lat = document.getElementById("Lat").innerHTML;
		document.getElementById("Lng").innerHTML = this.getPosition().lng();
		$scope.kmlFormData.Lng = document.getElementById("Lng").innerHTML;
	});
	
	google.maps.event.addListener(map_digitalize, 'maptypeid_changed', function() {
		document.getElementById("MapType").innerHTML = map_digitalize.getMapTypeId();
		$scope.kmlFormData.MapType = document.getElementById("MapType").innerHTML;
		
		if(map_digitalize.getMapTypeId() == 'hybrid') {
			var mapSource = document.getElementById('map_canvas_digitalize').getElementsByClassName('gmnoprint')[1].getElementsByTagName('span')[0].innerHTML;
			document.getElementById("ImgSource").innerHTML = mapSource.split('Imagery ')[1];
			$scope.kmlFormData.ImgSource = document.getElementById("ImgSource").innerHTML;
		}else {
			document.getElementById("ImgSource").innerHTML = '-';
			$scope.kmlFormData.ImgSource = document.getElementById("ImgSource").innerHTML;
		}
	
		var mapSource = document.getElementById('map_canvas_digitalize').getElementsByClassName('gmnoprint')[1].getElementsByTagName('span')[0].innerHTML;
		document.getElementById("MapSource").innerHTML = mapSource.split(mapSource[4])[1];
		$scope.kmlFormData.MapSource = document.getElementById("MapSource").innerHTML; 
		
	});

	google.maps.event.addListener(map_digitalize, 'zoom_changed', function() {
		var mapSource = document.getElementById('map_canvas_digitalize').getElementsByClassName('gmnoprint')[1].getElementsByTagName('span')[0].innerHTML;
		
		document.getElementById("ZoomLevel").innerHTML = map_digitalize.getZoom();
		document.getElementById("MapSource").innerHTML = mapSource.split(mapSource[4])[1];
		$scope.kmlFormData.ZoomLevel = document.getElementById("ZoomLevel").innerHTML;
		$scope.kmlFormData.MapSource = document.getElementById("MapSource").innerHTML;
		
		if(map_digitalize.getMapTypeId() == 'hybrid') {
			var mapSource = document.getElementById('map_canvas_digitalize').getElementsByClassName('gmnoprint')[1].getElementsByTagName('span')[0].innerHTML;
			document.getElementById("ImgSource").innerHTML = mapSource.split('Imagery ')[1].split(mapSource.split('Imagery')[1][1])[1];
			$scope.kmlFormData.ImgSource = document.getElementById("ImgSource").innerHTML;
		}
	});
	
	function initialize(kml) {
		
		if(kml['MapType'] == "roadmap")
			var mapType = google.maps.MapTypeId.ROADMAP;
		if(kml['MapType'] == "hybrid")
			var mapType = google.maps.MapTypeId.HYBRID;
			
		var myLatlng = new google.maps.LatLng(parseFloat(kml['Lat']), parseFloat(kml['Lng']));
		var mapOptions = {
		  center: myLatlng,
		  zoom: parseFloat(kml['ZoomLevel']),
		  mapTypeId: mapType
		};
		var map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
		var marker = new google.maps.Marker({
		  position: myLatlng,
		  map: map,
		  title: 'Marker'
		});

	}
	
	$('#showKmlMap').click(function() { $('.ui.modal').modal('setting', 'transition', 'vertical flip').modal('show'); initialize($scope.field); });
	
	
	$scope.$on("fileSelected", function (event, args) {
		$scope.$apply(function () {
			console.log(args);
			if(args.file.name.split(".")[1] != 'kml')
				toastr.error('請挑選.kml格式進行上傳.', 'Error Info');
			else
				$scope.files.push(args.file);
		});
	})
	 
	$scope.kmlProcessForm = function() {
		$http({
			method: 'POST',
			url: 'kmlProcess.php',
			transformRequest: function (data) {
				var formdata = new FormData();
				formdata.append("kmlFormData", angular.toJson(data.kmlFormData));
				formdata.append("kmlFile", data.kmlFile);

				/*
				for(var i=0; i <data.files; i++) {
					formdata.append("gpsFile"+i, data.files[i])
				}
				*/
				
				return formdata;
			},
			//data: $.param($scope.formData),
			data: {kmlFormData: $scope.kmlFormData, kmlFile: $scope.files[0] },
			headers : { 'Content-Type': false }
		})
			.success(function(data) {
				
				console.log(data);
				$scope.output = true;
				$scope.loading = false;
				$scope.field = data;
				document.getElementsByName('name')[0].value = '';
				document.getElementsByName('email')[0].value = '';
				document.getElementsByName('description')[0].value = '';
				toastr.success('您的KML資料已解析完成，請至下方Output標籤內觀看資訊.', 'Info');
				
			});
	};
	
});

myApp.controller('shpFormController', function($scope, $http) {
	$scope.shpFormData = {};
	$scope.files = [];
	
	$scope.output = false;
	$scope.loading = false;
	$scope.field = {};
	
	$('.ui.fluid.accordion').accordion();
	
	function initialize(shp) {
		
		
		var myLatlng = new google.maps.LatLng(parseFloat(shp.rowInfo[0].Lat), parseFloat(shp.rowInfo[0].Lng));
		var mapOptions = {
		  center: myLatlng,
		  zoom: 12,
		  mapTypeId: google.maps.MapTypeId.ROADMAP
		};
		var map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
		var marker = new google.maps.Marker({
		  position: myLatlng,
		  animation: google.maps.Animation.DROP,
		  map: map,
		  title: 'Marker'
		});
		 

	}
	
	$('#showShpMap').click(function() { $('.ui.modal').modal('setting', 'transition', 'vertical flip').modal('show'); initialize($scope.field); });
	
	$scope.$on("fileSelected", function (event, args) {
		$scope.$apply(function () {
			console.log($scope.files);
			if(args.file.name.split(".")[1] != 'shp' && args.file.name.split(".")[1] != 'shx' && args.file.name.split(".")[1] != 'dbf')
				toastr.error('請挑選 .shp、.shx、.dbf 格式進行上傳.', 'Error Info');
			else {
			if($scope.files.length > 0) {
				if($scope.files.length == 3) {
					toastr.error('已將.shp、.shx、.dbf 檔案上傳完畢.', 'Error Info');
				}else {
					if($scope.files[0].name.split(".")[0] == args.file.name.split(".")[0])
						$scope.files.push(args.file);
					else 
						toastr.error('請上傳相同檔名之 .shp、.shx、.dbf 檔案.', 'Error Info');
				}
			}else { 
				$scope.files.push(args.file);	
			}
			}
		});
	})
	 
	$scope.shpProcessForm = function() {
		$http({
			method: 'POST',
			url: 'shpProcess.php',
			transformRequest: function (data) {
				var formdata = new FormData();
				formdata.append("shpFormData", angular.toJson(data.shpFormData));
				//formdata.append("shpFile", data.shpFile);

				
				for(var i=0; i <3; i++) {
					var format = data.shpFile[i].name.split(".")[1];
					formdata.append("shpFile_"+format, data.shpFile[i]);
				}
			
				return formdata;
			},
			//data: $.param($scope.formData),
			data: {shpFormData: $scope.shpFormData, shpFile: $scope.files },
			headers : { 'Content-Type': false }
		})
			.success(function(data) {
				
				console.log(data);
				$scope.output = true;
				$scope.loading = false;
				$scope.field = data;
				toastr.success('您的SHAPEFILE資料已解析完成，請至下方Output標籤內觀看資訊.', 'Info');
				
			});
	};
	
});

myApp.controller('refFormController', function($scope, $http) {
	$scope.refFormData = {};
	$scope.files = [];
	
	$scope.output = false;
	$scope.field = {};
	
	$('.ui.fluid.accordion').accordion();
		
	var myLatlng = new google.maps.LatLng(22.994, 120.225);
	var mapOptions = {
	  center: myLatlng,
	  zoom: 12,
	  mapTypeId: google.maps.MapTypeId.ROADMAP
	};
	var map_digitalize = new google.maps.Map(document.getElementById("map_canvas_digitalize"), mapOptions);
	var marker_digitalize = new google.maps.Marker({
	  position: myLatlng,
	  draggable:true,
	  animation: google.maps.Animation.BOUNCE,
	  map: map_digitalize,
	  title: 'Place'
	});
	
	
	function update(e) {
		$scope.Lat = e.getPosition().lat();
		$scope.Lng = e.getPosition().lng();
		$scope.ZoomLevel = map_digitalize.getZoom();
		$scope.MapType = map_digitalize.getMapTypeId();
		$scope.ImgSource = '-';
		$scope.MapSource = '2014 Google';
		
		$scope.refFormData.Lat = e.getPosition().lat();
		$scope.refFormData.Lng = e.getPosition().lng();
		$scope.refFormData.ZoomLevel = map_digitalize.getZoom();
		$scope.refFormData.MapType = map_digitalize.getMapTypeId();
		$scope.refFormData.ImgSource = '-';
		$scope.refFormData.MapSource = '2014 Google';
	}
	
	update(marker_digitalize);
	
	google.maps.event.addListener(marker_digitalize, 'dragend', function() {
		map_digitalize.panTo(this.getPosition());
		
		document.getElementById("Lat").innerHTML = this.getPosition().lat();
		$scope.refFormData.Lat = document.getElementById("Lat").innerHTML;
		document.getElementById("Lng").innerHTML = this.getPosition().lng();
		$scope.refFormData.Lng = document.getElementById("Lng").innerHTML;
	});
	
	google.maps.event.addListener(map_digitalize, 'maptypeid_changed', function() {
		document.getElementById("MapType").innerHTML = map_digitalize.getMapTypeId();
		$scope.refFormData.MapType = document.getElementById("MapType").innerHTML;
		
		if(map_digitalize.getMapTypeId() == 'hybrid') {
			var mapSource = document.getElementById('map_canvas_digitalize').getElementsByClassName('gmnoprint')[1].getElementsByTagName('span')[0].innerHTML;
			document.getElementById("ImgSource").innerHTML = mapSource.split('Imagery ')[1];
			$scope.refFormData.ImgSource = document.getElementById("ImgSource").innerHTML;
		}else {
			document.getElementById("ImgSource").innerHTML = '-';
			$scope.refFormData.ImgSource = document.getElementById("ImgSource").innerHTML;
		}
	
		var mapSource = document.getElementById('map_canvas_digitalize').getElementsByClassName('gmnoprint')[1].getElementsByTagName('span')[0].innerHTML;
		document.getElementById("MapSource").innerHTML = mapSource.split(mapSource[4])[1];
		$scope.refFormData.MapSource = document.getElementById("MapSource").innerHTML; 
		
	});

	google.maps.event.addListener(map_digitalize, 'zoom_changed', function() {
		var mapSource = document.getElementById('map_canvas_digitalize').getElementsByClassName('gmnoprint')[1].getElementsByTagName('span')[0].innerHTML;
		
		document.getElementById("ZoomLevel").innerHTML = map_digitalize.getZoom();
		document.getElementById("MapSource").innerHTML = mapSource.split(mapSource[4])[1];
		$scope.refFormData.ZoomLevel = document.getElementById("ZoomLevel").innerHTML;
		$scope.refFormData.MapSource = document.getElementById("MapSource").innerHTML;
		
		if(map_digitalize.getMapTypeId() == 'hybrid') {
			var mapSource = document.getElementById('map_canvas_digitalize').getElementsByClassName('gmnoprint')[1].getElementsByTagName('span')[0].innerHTML;
			document.getElementById("ImgSource").innerHTML = mapSource.split('Imagery ')[1].split(mapSource.split('Imagery')[1][1])[1];
			$scope.refFormData.ImgSource = document.getElementById("ImgSource").innerHTML;
		}
	});
	
	function initialize(kml) {
		
		if(kml['MapType'] == "roadmap")
			var mapType = google.maps.MapTypeId.ROADMAP;
		if(kml['MapType'] == "hybrid")
			var mapType = google.maps.MapTypeId.HYBRID;
			
		var myLatlng = new google.maps.LatLng(parseFloat(kml['Lat']), parseFloat(kml['Lng']));
		var mapOptions = {
		  center: myLatlng,
		  zoom: parseFloat(kml['ZoomLevel']),
		  mapTypeId: mapType
		};
		var map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
		var marker = new google.maps.Marker({
		  position: myLatlng,
		  map: map,
		  title: 'Marker'
		});

	}
	
	$('#showRefMap').click(function() { $('.ui.modal').modal('setting', 'transition', 'vertical flip').modal('show'); initialize($scope.field); });
	
	
	$scope.$on("fileSelected", function (event, args) {
		$scope.$apply(function () {
			console.log(args);
			if(args.file.name.split(".")[1] != 'tif')
				toastr.error('請挑選.tif格式進行上傳.', 'Error Info');
			else
				$scope.files.push(args.file);
		});
	})
	 
	$scope.refProcessForm = function() {
		$http({
			method: 'POST',
			url: 'ref2Process.php',
			transformRequest: function (data) {
				var formdata = new FormData();
				formdata.append("refFormData", angular.toJson(data.refFormData));

				/*
				for(var i=0; i <data.files; i++) {
					formdata.append("gpsFile"+i, data.files[i])
				}
				*/
				
				return formdata;
			},
			//data: $.param($scope.formData),
			data: {refFormData: $scope.refFormData},
			headers : { 'Content-Type': false }
		})
			.success(function(data) {
				
				console.log(data);
				$scope.output = true;
				$scope.loading = false;
				$scope.field = data;
				toastr.success('您的數化資料已解析完成，請至下方Output標籤內觀看資訊.', 'Info');
				
			});
	};
	
	$scope.refDigitalizeForm = function() {
		$http({
			method: 'POST',
			url: 'refProcess.php',
			transformRequest: function (data) {
				var formdata = new FormData();
				formdata.append("refDigiFile", data.refDigiFile);
				
				return formdata;
			},
			//data: $.param($scope.formData),
			data: {refDigiFile: $scope.files[0] },
			headers : { 'Content-Type': false }
		})
			.success(function(data) {
				
				console.log(data);
				$scope.output = false;
				$scope.Digiloading = false;
				$scope.field = data;
				toastr.info('您的數化資料已解析完成，請至Map內觀看數化底圖.', 'Info');
				
				var overlay;
				
				var imageBounds = new google.maps.LatLngBounds(
					new google.maps.LatLng(parseFloat(data.LowerLeft[1]), parseFloat(data.LowerLeft[0])),
					new google.maps.LatLng(parseFloat(data.UpperRight[1]), parseFloat(data.UpperRight[0]))
				);
				var imgpath = 'http://140.116.47.109/vgi_web/'+data.path;
				overlay = new google.maps.GroundOverlay(
					imgpath,
					imageBounds
				);
				overlay.setMap(map_digitalize);
				
				if($scope.field.LowerLeft) {
					$scope.refFormData.LowerLeft = $scope.field.LowerLeft;
					document.getElementById("UL").innerHTML = $scope.field.LowerLeft[1]+','+$scope.field.LowerLeft[0];
					$scope.refFormData.UpperRight = $scope.field.UpperRight;
					document.getElementById("LR").innerHTML = $scope.field.UpperRight[1]+','+$scope.field.UpperRight[0];
					$scope.refFormData.path = $scope.field.path;
				}
			});
	};
});