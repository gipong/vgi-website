﻿// gpx preview part
function previewGpxFile(evt) {
	evt.stopPropagation();
	evt.preventDefault();
	
	var files = evt.dataTransfer.files;
	console.log(files.name);
	
}

function dragOverGpx() {
	evt.stopPropagation();
	evt.preventDefault();
	evt.dataTransfer.dropEffect = 'copy';
}

var dropZone = document.getElementById('preGpx');
dropZone.addEventListener('dragover', dragOverGpx, false);
dropZone.addEventListener('drop', previewGpxFile, false);
	
	