<?php
/*
	$errors = array();
	$data = array();
	if(empty($_POST['gpsFormData']))
		$errors['gpsFormData'] = 'get';
	if (empty($_POST['email']))
		$errors['email'] = 'Name is required.';

	if (empty($_POST['password']))
		$errors['password'] = 'Password is required.';

	if (!empty($errors)) {

		// if there are items in our errors array, return those errors
		$data['success'] = false;
		$data['errors']  = $errors;
	} else {

		// if there are no errors, return a message
		$data['success'] = true;
		$data['message'] = 'Success!';
	}
*/
	
	move_uploaded_file($_FILES['gpsFile']['tmp_name'], "upload/".$_FILES['gpsFile']['name']);
	
/*
	$output = print_r($_FILES, true);
	file_put_contents('file.txt', $output);
	// return all our data to an AJAX call
	echo json_encode($output);
*/
	$gpx = simplexml_load_file("upload/".$_FILES['gpsFile']['name']);
	$gpxInfo = array();
	
	$gpsFormData = json_decode($_POST['gpsFormData']);
	
	$gpxInfo['id'] = sha1($gpsFormData->email.substr($gpsFormData->password, 0, 3));
	$gpxInfo['creator'] = $gpx->attributes()->creator;
	$gpxInfo['version'] = $gpx->attributes()->version;
	if($gpx->time) $gpxInfo['time'] = $gpx->time;
	if($gpx->bounds) {
		$gpxInfo['bounds'] = array();
		$gpxInfo['bounds']['minlat'] = $gpx->bounds->attributes()->minlat;
		$gpxInfo['bounds']['minlon'] = $gpx->bounds->attributes()->minlon;
		$gpxInfo['bounds']['maxlat'] = $gpx->bounds->attributes()->maxlat;
		$gpxInfo['bounds']['maxlon'] = $gpx->bounds->attributes()->maxlon;
	}
	
	if($gpx->rte) {
		$rte = $gpx->rte;
	
		$rtept = $rte->rtept;
		$rteptCount = count($rtept);
		$gpxInfo['rte'] = array();
		$gpxInfo['rteName'] = (string)$rte->name;
		$gpxInfo['rteCount'] = $rteptCount;
		
		for($i = 0; $i < $rteptCount; $i++) {
			
			array_push($gpxInfo['rte'], array("lat"=>(string)$rtept[$i]->attributes()->lat, "lon"=>(string)$rtept[$i]->attributes()->lon));
		}
		
		echo json_encode($gpxInfo);
	}
	else if($gpx->trk) {
		$trk = $gpx->trk;
		
		$trkpt = $trk->trkseg->trkpt;
		$trkptCount = count($trkpt);
		$gpxInfo['trk'] = array();
		$gpxInfo['trkName'] = 'Track';
		$gpxInfo['trkCount'] = $trkptCount;
		
		for($i = 0; $i < $trkptCount; $i++) {
			
			array_push($gpxInfo['trk'], array("lat"=>(string)$trkpt[$i]->attributes()->lat, "lon"=>(string)$trkpt[$i]->attributes()->lon));
		}
		
		echo json_encode($gpxInfo);
	}